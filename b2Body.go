package box2d_go

const (
	B2BodyType_StaticBody    = 0
	B2BodyType_KinematicBody = 1
	B2BodyType_DynamicBody   = 2
)

type B2BodyDef struct {
	Type            int
	Position        *B2Vec2
	Angle           float32
	LinearVelocity  *B2Vec2
	AngularVelocity float32
	LinearDamping   float32
	AngularDamping  float32
	AllowSleep      bool
	Awake           bool
	FixedRotation   bool

	Bullet       bool
	Active       bool
	UserData     interface{}
	GravityScale float32
}

func NewB2BodyDef() *B2BodyDef {
	retBodyDef := &B2BodyDef{}
	retBodyDef.UserData = nil
	retBodyDef.Position.Set(0.0, 0.0)
	retBodyDef.Angle = 0.0
	retBodyDef.LinearVelocity.Set(0.0, 0.0)
	retBodyDef.AngularVelocity = 0.0
	retBodyDef.LinearDamping = 0.0
	retBodyDef.AngularDamping = 0.0
	retBodyDef.AllowSleep = true
	retBodyDef.Awake = true
	retBodyDef.FixedRotation = false
	retBodyDef.Bullet = false
	retBodyDef.Type = B2BodyType_StaticBody
	retBodyDef.Active = true
	retBodyDef.GravityScale = 1.0
	return retBodyDef
}

type B2Body struct {
}
