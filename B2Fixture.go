package box2d_go

type B2Filter struct {
	CategoryBits uint16
	MaskBits     uint16
	GroupIndex   int16
}

func NewB2Filter() *B2Filter {
	retfilter := &B2Filter{}
	retfilter.CategoryBits = 0x0001
	retfilter.MaskBits = 0xFFFF
	retfilter.GroupIndex = 0
	return retfilter
}

type B2FixtureProxy struct {
	AABB       *B2AABB
	Fixture    *B2Fixture
	ChildIndex int32
	ProxyId    int32
}

type B2Fixture struct {
	m_density     float32
	m_next        *B2Fixture
	m_body        *B2Body
	m_shape       IB2Shape
	m_friction    float32
	m_restitution float32

	m_proxies    *B2FixtureProxy
	m_proxyCount int32
	m_isSensor   bool
	m_userData   interface{}
}

func (this *B2Fixture) GetType() int {
	return this.m_shape.GetType()
}

func (this *B2Fixture) GetShape() IB2Shape {
	return this.m_shape
}
