package box2d_go

const (
	B2ShapeType_Circle    = 0
	B2ShapeType_Edge      = 1
	B2ShapeType_Polygon   = 2
	B2ShapeType_Chain     = 3
	B2ShapeType_TypeCount = 4
)

type B2MassData struct {
	Mass   float32
	Center float32
	I      float32
}

type B2ShapeInfo struct {
	m_type   int
	m_radius float32
}

func (this *B2ShapeInfo) GetType() int {
	return this.m_type
}

type IB2Shape interface {
	GetType() int
	TestPoint(xf *B2Transform, p *B2Transform) bool
	RayCast(*B2RayCastOutput, *b2RayCastInput, *B2Transform, int32) bool
	ComputeAABB(*B2AABB, *B2Transform, int32)
	ComputeMass(*B2MassData, float32)
}
