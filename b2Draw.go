package box2d_go

type B2Color struct {
	R float32
	G float32
	B float32
	A float32
}

func NewB2Color(rIn, gIn, bIn, aIn float32) {
	retColor := &B2Color{}
	retColor.R = rIn
	retColor.G = gIn
	retColor.B = bIn
	retColor.A = aIn
}
func (this *B2Color) Set(rIn, gIn, bIn, aIn float32) {
	this.R = rIn
	this.G = gIn
	this.B = bIn
	this.A = aIn
}
