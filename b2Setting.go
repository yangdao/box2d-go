package box2d_go

import (
	"fmt"
	"math"
)

const (
	b2_pi       = 3.14159265359
	b2_maxFloat = math.MaxFloat32
	b2_epsilon  = 1.192092896e-07

	// Collision

	/// The maximum number of contact points between two convex shapes. Do
	/// not change this value.
	b2_maxManifoldPoints = 2

	/// The maximum number of vertices on a convex polygon. You cannot increase
	/// this too much because b2BlockAllocator has a maximum object size.
	b2_maxPolygonVertices = 8

	/// This is used to fatten AABBs in the dynamic tree. This allows proxies
	/// to move by a small amount without triggering a tree adjustment.
	/// This is in meters.
	b2_aabbExtension = 0.1

	/// This is used to fatten AABBs in the dynamic tree. This is used to predict
	/// the future position based on the current displacement.
	/// This is a dimensionless multiplier.
	b2_aabbMultiplier = 2.0

	/// A small length used as a collision and constraint tolerance. Usually it is
	/// chosen to be numerically significant, but visually insignificant.
	b2_linearSlop = 0.005

	/// A small angle used as a collision and constraint tolerance. Usually it is
	/// chosen to be numerically significant, but visually insignificant.
	b2_angularSlop = (2.0 / 180.0 * b2_pi)

	/// The radius of the polygon/edge shape skin. This should not be modified. Making
	/// this smaller means polygons will have an insufficient buffer for continuous collision.
	/// Making it larger may create artifacts for vertex collision.
	b2_polygonRadius = (2.0 * b2_linearSlop)

	/// Maximum number of sub-steps per contact in continuous physics simulation.
	b2_maxSubSteps = 8

	// Dynamics

	/// Maximum number of contacts to be handled to solve a TOI impact.
	b2_maxTOIContacts = 32

	/// A velocity threshold for elastic collisions. Any collision with a relative linear
	/// velocity below this threshold will be treated as inelastic.
	b2_velocityThreshold = 1.0

	/// The maximum linear position correction used when solving constraints. This helps to
	/// prevent overshoot.
	b2_maxLinearCorrection = 0.2

	/// The maximum angular position correction used when solving constraints. This helps to
	/// prevent overshoot.
	b2_maxAngularCorrection = (8.0 / 180.0 * b2_pi)

	/// The maximum linear velocity of a body. This limit is very large and is used
	/// to prevent numerical problems. You shouldn't need to adjust this.
	b2_maxTranslation        = 2.0
	b2_maxTranslationSquared = (b2_maxTranslation * b2_maxTranslation)
	/// The maximum angular velocity of a body. This limit is very large and is used
	/// to prevent numerical problems. You shouldn't need to adjust this.
	b2_maxRotation        = (0.5 * b2_pi)
	b2_maxRotationSquared = (b2_maxRotation * b2_maxRotation)
	/// This scale factor controls how fast overlap is resolved. Ideally this would be 1 so
	/// that overlap is removed in one time step. However using values close to 1 often lead
	/// to overshoot.
	b2_baumgarte   = 0.2
	b2_toiBaugarte = 0.75
	// Sleep
	/// The time that a body must be still before it will go to sleep.
	b2_timeToSleep = 0.5
	/// A body cannot sleep if its linear velocity is above this tolerance.
	b2_linearSleepTolerance = 0.01
	/// A body cannot sleep if its angular velocity is above this tolerance.
	b2_angularSleepTolerance = (2.0 / 180.0 * b2_pi)
)

func b2Log(args ...interface{}) {
	fmt.Println(args...)
}

type b2Version struct {
	major    int32 ///< significant changes
	minor    int32 ///< incremental changes
	revision int32 ///< bug fixes
}

var b2_version = b2Version{2, 3, 2}
