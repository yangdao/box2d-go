package box2d_go

const (
	B2JoinType_UnknownJoint   = 0
	B2JoinType_RevoluteJoint  = 1
	B2JoinType_PrismaticJoint = 2
	B2JoinType_DistanceJoint  = 3
	B2JoinType_PulleyJoint    = 4
	B2JoinType_MouseJoint     = 5
	B2JoinType_GearJoint      = 6
	B2JoinType_WheelJoint     = 7
	B2JoinType_WeldJoint      = 8
	B2JoinType_FrictionJoint  = 9
	B2JoinType_RopeJoint      = 10
	B2JoinType_MotorJoint     = 11
)

type B2JointDef struct {
	Type             int
	UserData         interface{}
	BodyA            *B2Body
	BodyB            *B2Body
	CollideConnected bool
}
